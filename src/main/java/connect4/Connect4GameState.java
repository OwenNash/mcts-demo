package connect4;

import mcts.GameState;

import java.util.ArrayList;
import java.util.List;

public class Connect4GameState implements GameState {
    private char[][] grid;
    private char player = 'R';
    private char opponent = 'b';
    private boolean active = true;

    public Connect4GameState(char[][] grid) {
        this.grid = grid;
    }


    public int getWinner() {
        if (Connect4.isWinner(player, grid)) return 1;
        if (Connect4.isWinner(opponent, grid)) return -1;
        return 0;
    }

    public int getCurrentPlayer() {
        return active ? 1 : -1;
    }

    public int getNumberOfAvailableMoves() {
        return validColumns().size();
    }

    public void executeMove(int move) {
        Connect4.dropCounter(active ? player : opponent, validColumns().get(move), grid);
        active = !active;
    }

    public List<Integer> validColumns() {
        List<Integer> valid = new ArrayList<Integer>();
        for (int i = 0; i < 7; i++) {
            if (Connect4.validate(i, grid)) valid.add(i);
        }
        return valid;
    }

    public GameState clone() {
        char[][] current = new char[6][7];
        for (int row = 0; row < grid.length; row++){
            for (int col = 0; col < grid[0].length; col++){
                current[row][col] = grid[row][col];
            }
        }
        return new Connect4GameState(current);
    }
}
