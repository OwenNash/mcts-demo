/**
 * 
 */
package chess.player;


import chess.Board;
import chess.Move;
import chess.minimax.MinimaxAlphaBeta;
import mcts.ChessGameState;
import mcts.MonteCarloTreeSearch;

/**
 * @author Gunnar Atli
 *
 */
public class MctsPlayer extends Player {
	private	MonteCarloTreeSearch mcts = new MonteCarloTreeSearch();

	public MctsPlayer(boolean color) {
		super(color);
	}

	public Move getNextMove(Board b) {
		return b.getMoves(color).get(mcts.selectMove(new ChessGameState(b, color), 100));
	}

}
