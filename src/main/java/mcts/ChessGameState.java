package mcts;

import chess.Board;

public class ChessGameState implements GameState {
    private Board board;
    private boolean player;

    public ChessGameState(Board board, boolean player) {
        this.board = board;
        this.player = player;
    }

    public int getWinner() {
        if (board.getMoves(player).size() == 0) {
            return player ? -1 : 1;
        }
        return 0;
    }

    public int getCurrentPlayer() {
        return player ? 1 : -1;
    }

    public int getNumberOfAvailableMoves() {
        return board.getMoves(player).size();
    }

    public void executeMove(int move) {
        board.makeMove(board.getMoves(player).get(move));
        player = !player;
    }

    public GameState clone() {
        return new ChessGameState(new Board(board.tiles), player);
    }
}
