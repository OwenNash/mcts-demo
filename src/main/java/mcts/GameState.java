package mcts;

public interface GameState {
    /**
     * @return 0 if no winner, 1 is player wins, -1 if player loses
     */
    int getWinner();

    /**
     * @return 1 is player is to act, -1 if opponent is to act
     */
    int getCurrentPlayer();

    int getNumberOfAvailableMoves();

    void executeMove(int move);

    GameState clone();
}
