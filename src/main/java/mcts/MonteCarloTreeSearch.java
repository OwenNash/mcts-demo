package mcts;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MonteCarloTreeSearch {
    private static final float EXPLORATION_CONSTANT = 1.4f;

    public int selectMove(GameState originalState, int iterations) {
        Node root = new Node(-1);
        root.addChildren(originalState.getNumberOfAvailableMoves());
        for (int i = 0; i < iterations; i++) {
            GameState state = originalState.clone();
            List<Node> visitedNodes = new ArrayList<Node>();
            visitedNodes.add(root);
            Node selectedNode = root;
            boolean simulate = true;
            do {
                selectedNode = selectedNode.selectNode(state.getCurrentPlayer() == 1, EXPLORATION_CONSTANT);
                visitedNodes.add(selectedNode);
                if (state.getNumberOfAvailableMoves() == 0) {
                    System.out.println("hmmm");
                }
                state.executeMove(selectedNode.move);
                if (state.getWinner() == 1) {
                    recordVisits(visitedNodes, 1);
                    simulate = false;
                    break;
                } else if (state.getWinner() == -1) {
                    recordVisits(visitedNodes, 0);
                    simulate = false;
                    break;
                } else if (state.getNumberOfAvailableMoves() == 0) {
                    recordVisits(visitedNodes, 0.5f);
                    simulate = false;
                    break;
                }
            } while (selectedNode.visits > 0);
            if (simulate) {
                selectedNode.addChildren(state.getNumberOfAvailableMoves());
                recordVisits(visitedNodes, simulateToTerminalState(state));
            }
        }
        Node node = root.selectNode(true, 0);
        System.out.println("Confidence in victory: " + node.getUtility(true));
        return node.move;
    }

    private float simulateToTerminalState(GameState state) {
        while (state.getWinner() == 0 && state.getNumberOfAvailableMoves() > 0) {
            state.executeMove(new Random().nextInt(state.getNumberOfAvailableMoves()));
        }
        if (state.getWinner() == 1) {
            return 1;
        }
        if (state.getWinner() == -1) {
            return 0;
        }
        return 0.5f;
    }

    private void recordVisits(List<Node> nodes, float score) {
        for (Node node : nodes) {
            node.visit(score);
        }
    }

    private class Node {
        private List<Node> children = new ArrayList<Node>();
        private int visits = 0;
        private double wins = 0;
        private int move;

        Node(int move) {
            this.move = move;
        }

        void addChildren(int number) {
            for (int i = 0; i < number; i++) children.add(new Node(i));
        }

        void visit(float score) {
            visits++;
            wins += score;
        }

        Node selectNode(boolean maximise, float explorationConstant) {
            double bestScore = -1;
            List<Node> nodes = new ArrayList<Node>();
            for (Node child : children) {
                double score = child.getScore(visits, maximise, explorationConstant);
                if (score - bestScore > 0.00001) {
                    nodes = new ArrayList<Node>();
                    nodes.add(child);
                    bestScore = score;
                } else if (score - bestScore > -0.00001) {
                    nodes.add(child);
                }
            }
            if (nodes.size() == 0) {
                throw new RuntimeException("Zero nodes selected...");
            }
            int i = (int) Math.floor(new Random().nextInt(nodes.size()));
            return nodes.get(i);
        }

        double getScore(int parentVisits, boolean maximise, float explorationConstant) {
            if (visits == 0) return Integer.MAX_VALUE;
            return getUtility(maximise) + (explorationConstant * Math.sqrt(Math.log(parentVisits) / visits));
        }

        double getUtility(boolean maximise) {
            double utility = (wins * 1.0 / visits);
            if (maximise) return utility;
            return 1.0 - utility;
        }
    }
}
